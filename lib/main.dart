// Flutter code sample for widgets.Navigator.1

// The following example demonstrates how a nested [Navigator] can be used to
// present a standalone user registration journey.
//
// Even though this example uses two [Navigator]s to demonstrate nested
// [Navigator]s, a similar result is possible using only a single [Navigator].
//
// Run this example with `flutter run --route=/signup` to start it with
// the signup flow instead of on the home page.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      title: 'Code Sample for Bottom Navigation',
      // MaterialApp contains our top-level Navigator
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => BottomNavigator(),
      },
    );
  }
}

class BottomNavigator extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => BottomNavigatorState();
}

class BottomNavigatorState extends State<BottomNavigator> {

  TabItem selectedTab = TabItem.a;

  Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys = {
    TabItem.a: GlobalKey<NavigatorState>(),
    TabItem.b: GlobalKey<NavigatorState>(),
    TabItem.c: GlobalKey<NavigatorState>(),
  };

  void _selectTab(TabItem tabItem) {
    setState(() {
      selectedTab = tabItem;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !await navigatorKeys[selectedTab].currentState.maybePop(),
      child: Scaffold(
        body: Stack(children: <Widget>[
          _buildNavigator(TabItem.a),
          _buildNavigator(TabItem.b),
          _buildNavigator(TabItem.c),
        ]),
        bottomNavigationBar: BottomNavigation(
            selectedTab: selectedTab,
            onSelectTab: _selectTab
        ),
      ),
    );
  }

  Widget _buildNavigator(TabItem tabItem) {
    return Offstage(
      offstage: selectedTab != tabItem,
      child: Navigator(
        key: navigatorKeys[tabItem],
        initialRoute: "/",
        onGenerateRoute: (settings) {
          WidgetBuilder builder = (BuildContext _) => PageExample(tabItem: tabItem, depth: 0);
          return MaterialPageRoute(builder: builder, settings: settings);
        },
      ),
    );
  }
}

enum TabItem { a, b, c }

class BottomNavigation extends StatelessWidget {

  final TabItem selectedTab;
  final ValueChanged<TabItem> onSelectTab;

  BottomNavigation({this.selectedTab, this.onSelectTab});

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(items: [
      _buildItem(TabItem.a),
      _buildItem(TabItem.b),
      _buildItem(TabItem.c),
    ],
      onTap: (index) => onSelectTab(BottomTabHelper.item(index)),
    );
  }

  BottomNavigationBarItem _buildItem(TabItem tabItem) {
    return BottomNavigationBarItem(
      icon: Icon(
        _iconForTab(tabItem),
        color: _colorForTab(tabItem),
      ),
      title: Text(
        BottomTabHelper.description(tabItem),
        style: TextStyle(
          color: _colorForTab(tabItem),
        ),
      ),
    );
  }


  Color _colorForTab(TabItem tab) {
    return selectedTab == tab ? Colors.green : Colors.grey;
  }

  IconData _iconForTab(TabItem tab) {
    return selectedTab == tab ? Icons.lens : Icons.panorama_fish_eye;
  }
}


class BottomTabHelper {

  static TabItem item(int index) {
    switch (index) {
      case 0:
        return TabItem.a;
      case 1:
        return TabItem.b;
      case 2:
        return TabItem.c;
    }
    return TabItem.a;
  }

  static String description(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.a:
        return 'A';
      case TabItem.b:
        return 'B';
      case TabItem.c:
        return 'C';
    }
    return '';
  }

  static String initialRoute(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.a:
        return '/a';
      case TabItem.b:
        return '/b';
      case TabItem.c:
        return '/c';
    }
    return '/';
  }
}

class PageExample extends StatelessWidget {

  final TabItem tabItem;
  final int depth;

  PageExample({this.tabItem, this.depth});

  @override
  Widget build(BuildContext context) {
    String screenName = BottomTabHelper.description(tabItem);
    return Scaffold(
      appBar: AppBar(
        title: Text("Navigator $screenName"),
      ),
      body: _buildBody(screenName, context),
    );
  }

  Widget _buildBody(String screenName, BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
            child: Align(
              alignment: FractionalOffset.center,
              child: Center(
                  child: RichText(
                    text: TextSpan(
                      text: "Hi, I am a route\ninside the navigator named ",
                      style: TextStyle(color: Colors.blueGrey[800],  fontSize: 21.0, fontWeight:  FontWeight.w100),
                      children: <TextSpan>[
                        TextSpan(
                          text: screenName,
                          style: TextStyle(color: Colors.green,  fontSize: 21.0, fontWeight:  FontWeight.w700),
                        ),
                        TextSpan(
                            text: "\nCurrent navigation stack depth is  ",
                            style: TextStyle(color: Colors.blueGrey[800],  fontSize: 21.0, fontWeight:  FontWeight.w100),
                            children: <TextSpan>[
                              TextSpan(
                                text: "$depth",
                                style: TextStyle(color: Colors.deepOrangeAccent,  fontSize: 21.0, fontWeight:  FontWeight.w700),
                              ),
                            ]
                        ),
                      ],
                    ),
                  )
              ),
            )
        ),
        Positioned(
          child: Align(
            alignment: FractionalOffset(0.5, 0.99),
            child: FloatingActionButton.extended(
                label: Text("PUSH"),
                onPressed: _push(context)
            ),
          ),
        ),
      ],
    );
  }

  VoidCallback _push(BuildContext context) {
    return () =>
        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) => PageExample(
                    tabItem: tabItem,
                    depth: depth + 1
                )
            )
        );
  }
}